<div id="ocf-contact-form">
    <h1>Contact Form</h1>

    <?php //error message ?>
    <div class="ocf-message ocf-error" style="<?php echo empty($ocf_errors) ? 'display:none;' : '' ?>">
        <p>Sorry, there was a problem submitting the form. Please see the error(s) below.</p>
        <ul>
        <?php if(!empty($ocf_errors)): ?>
            <?php foreach($ocf_errors as $ocf_error): ?>
                <li><?php echo $ocf_error ?></li>
            <?php endforeach; ?>
        <?php endif; ?>
        </ul>
    </div>

    <?php //success message ?>
    <?php if(!empty($_POST['contact']) && empty($ocf_errors)): ?>
        <div class="ocf-message ocf-success">
            <p>Thanks for your submission.</p>
        </div>
        <?php $_POST['contact'] = ''; ?>
    <?php endif; ?>

    <form method="post" action="#ocf-contact-form">
        <?php wp_nonce_field('submit_ocf'); ?>
        <input type="hidden" name="action" value="organic_contact_form">
        <input type="hidden" name="contact[page_url]" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
        <label for="contact_name">Name<span class="required">*</span></label>
        <input type="text" name="contact[name]" id="contact_name" placeholder="Your name" 
            value="<?php echo isset($_POST['contact']['name']) ? $_POST['contact']['name'] : '' ?>" required maxlength="100">
        <label for="contact_email">Email<span class="required">*</span></label>
        <input type="email" name="contact[email]" id="contact_email" placeholder="Your email"
            value="<?php echo isset($_POST['contact']['email']) ? $_POST['contact']['email'] : '' ?>" required maxlength="255">
        <label for="contact_tel">Tel</label>
        <input type="text" name="contact[tel]" id="contact_tel" placeholder="Your telephone number"
            value="<?php echo isset($_POST['contact']['tel']) ? $_POST['contact']['tel'] : '' ?>" maxlength="20">
        <label for="contact_enquiry">Enquiry<span class="required">*</span></label>
        <textarea name="contact[enquiry]" id="contact_enquiry" placeholder="Your enquiry" required
            ><?php echo isset($_POST['contact']['enquiry']) ? $_POST['contact']['enquiry'] : '' ?></textarea>
        <button type="submit">Submit</button>
    </form>
</div>