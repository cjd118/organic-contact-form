<?php

//meta boxes
function ocf_add_meta_boxes(){
    add_meta_box('ocf_meta', 'Contact Form Submission', 'ocf_populate_meta_box', 'ocf_contact', 'normal');
    remove_meta_box( 'submitdiv', 'ocf_contact', 'side' );
}
function ocf_populate_meta_box($post){ ?>
    <table>
        <tr>
            <td>Name:</td>
            <td><?php echo $post->post_title ?></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><?php echo get_post_meta($post->ID, 'email', true) ?></td>
        </tr>
        <tr>
            <td>Telephone:</td>
            <td><?php echo get_post_meta($post->ID, 'tel', true) ?></td>
        </tr>
        <tr>
            <td>Message:</td>
            <td><?php echo $post->post_content ?></td>
        </tr>
        <tr>
            <td>Page URL:</td>
            <td><?php echo get_post_meta($post->ID, 'page_url', true) ?></td>
        </tr>           
        <tr>
            <td>Submitted:</td>
            <td><?php echo $post->post_date ?></td>
        </tr>                                       
    </table>
<?php }
add_action( 'add_meta_boxes', 'ocf_add_meta_boxes' );

//remove row action links
function ocf_row_actions($actions, $post){
    if($post->post_type == 'ocf_contact'){
        return [];
    }
    return $actions;
}
add_filter('post_row_actions', 'ocf_row_actions', 10, 2);

//bulk actions - remove edit
function ocf_bulk_actions($actions, $post){
        unset($actions['edit']);
        return $actions;
}
add_filter('bulk_actions-edit-ocf_contact','ocf_bulk_actions');

//export csv link
add_filter('views_edit-ocf_contact', 'ocf_add_export_link');
function ocf_add_export_link($views){
    $views['export'] = '<a target="_blank" href="' . admin_url( 'admin-post.php?action=ocf_contact_export' ) .'" class="primary">Export all as CSV</a>';
    return $views;
}

//export csv action
add_action('admin_post_ocf_contact_export', 'ocf_contact_export');
function ocf_contact_export(){
    if (!current_user_can('manage_options')){
        return;
    }
    $csv = "date,name,email,tel,message,page_url\n";    
    $posts = get_posts(array(
        'post_type' => 'ocf_contact',
        'posts_per_page' => -1,
    ));
    foreach($posts as $post){
        $csv .= '"' . $post->post_date . '",' 
        . '"' . $post->post_title . '",' 
        . '"' . get_post_meta($post->ID, 'email', true) . '",'
        . '"' . get_post_meta($post->ID, 'tel', true) . '",'
        . '"' . $post->post_content . '",'
        . '"' . get_post_meta($post->ID, 'page_url', true) . '"' . "\n";
    }

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename=ocf_contact_export.csv');
    header('Pragma: no-cache');

    echo $csv;
}

//column layout
add_filter('get_user_option_screen_layout_ocf_contact', 'ocf_set_screen_layout' );
function ocf_set_screen_layout(){
    return 1;
}