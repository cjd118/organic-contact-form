<?php

function register_assets() {
    wp_register_style('organic_contact_form_css', plugins_url('../css/organic-contact-form.css', __FILE__));
    wp_enqueue_style('organic_contact_form_css');
    wp_register_script( 'organic_contact_form_js', plugins_url('../js/dist/script.js',__FILE__ ), array('jquery'), '1.0',true);
    wp_enqueue_script('organic_contact_form_js');
}
add_action('init','register_assets');