<?php

if(isset($_POST['contact'])){
    if(!wp_verify_nonce($_REQUEST['_wpnonce'],'submit_ocf')){
        wp_nonce_ays();
    }
    $submission = $_POST['contact'];
    //sanitize
    $submission['name'] = sanitize_text_field($submission['name']);
    $submission['email'] = sanitize_email($submission['email']);
    $submission['tel'] = sanitize_text_field($submission['tel']);
    $submission['enquiry'] = sanitize_text_field($submission['enquiry']);
    $submission['page_url'] = esc_url_raw($submission['page_url']);
    //validate
    $ocf_errors = array();
    if(empty($submission['name'])){
        $ocf_errors[] = 'The name field was missing';
    }
    if(empty($submission['email'])){
        $ocf_errors[] = 'The email field was missing';
    }
    if(empty($submission['enquiry'])){
        $ocf_errors[] = 'The enquiry field was missing';
    }
    if(!is_email($submission['email'])){
        $ocf_errors[] = 'The email address was invalid';
    }

    //on success
    if(empty($ocf_errors)){
        wp_insert_post(array(
            'ID' => 0,
            'post_content' => $submission['enquiry'],
            'post_title' => $submission['name'],
            'post_status' => 'publish',
            'post_type' => 'ocf_contact',
            'meta_input' => array(
                'email' => $submission['email'],
                'tel' => $submission['tel'],
                'page_url' => $submission['page_url'],
            ),
        ));
    }
}