<?php

function display_form(){
    global $ocf_errors;
    ob_start();
    include plugin_dir_path( __FILE__ ) . 'form.php';
    return ob_get_clean();
}
add_shortcode('organic-contact-form', 'display_form');