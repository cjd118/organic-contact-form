<?php

//cpt
function organic_contact_form_submission() {

    $labels = array(
        'name'                  => 'Contact Form Submissions',
        'singular_name'         => 'Contact Form Submission',
        'edit_item'             => 'Submission',
    );
    $args = array(
        'labels'                => $labels,
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,       
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'rewrite'               => false,
        'capability_type'       => 'page',
        'capabilities'          => array(
                                    'create_posts' => false,
                                ),      
        'menu_icon'             => 'dashicons-megaphone',
        'supports'              => false,
        'map_meta_cap'          => true
    );
    register_post_type('ocf_contact', $args);

}
add_action('init', 'organic_contact_form_submission', 0);