<?php 

/*
Plugin Name: Organic Contact Form
Plugin URI: 
Description: A shortcode based contact form. 
Author: Chris Davis
Version: 1
Author URI: planetcjd.co.uk
*/

//contact form cpt
include plugin_dir_path( __FILE__ ) . 'includes/cpt.php';

//js/css inclusion
include plugin_dir_path( __FILE__ ) . 'includes/assets.php';

//admin setup
include plugin_dir_path( __FILE__ ) . 'includes/admin.php';

//these files need to be loaded later in the process
function loadAtInit()
{
    global $ocf_errors;
    //form submission
    include plugin_dir_path( __FILE__ ) . 'includes/form_submit.php';
    //shortcode
    include plugin_dir_path( __FILE__ ) . 'includes/shortcode.php';
}
add_action('init', 'loadAtInit');