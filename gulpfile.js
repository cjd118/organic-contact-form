var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

gulp.task('default', ['sass', 'js']);

gulp.task('sass', function(){
    return gulp.src('./scss/organic-contact-form.scss')
    .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
});

gulp.task('js', function(){
    return gulp.src('./js/src/script.js')
    .pipe(sourcemaps.init())
        .pipe(uglify({mangle: false}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./js/dist'));
});

gulp.task('watch', function() {
    gulp.watch('scss/**/*.scss',['sass']);
    gulp.watch('js/src/script.js',['js']);
});