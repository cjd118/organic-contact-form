(function($) {

    var ocf_errors;

    $('#ocf-contact-form form').on('submit', function(e){
        ocf_errors = [];
        if(_isEmpty('#contact_name')){
            ocf_errors.push('The name field was missing');
        }
        if(_isEmpty('#contact_email')){
            ocf_errors.push('The email field was missing');
        }
        if(_isEmpty('#contact_enquiry')){
            ocf_errors.push('The enquiry field was missing');
        }
        if(!_isEmpty('#contact_email') && !_isEmail('#contact_email')){
            ocf_errors.push('The email address was invalid');
        }

        if(ocf_errors.length > 0){
            _showErrors(ocf_errors);
            e.preventDefault();
        }
    });

    function _isEmpty(input){
        return $.trim( $(input).val() ).length==0 ? true : false;
    }
    function _isEmail(input){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test($.trim( $(input).val() ));
    }
    function _showErrors(errors){
        $('.ocf-success').hide();
        $('.ocf-error').slideDown('fast');
        $('.ocf-error ul').empty();
        $.each(errors, function(i,v){
            $('.ocf-error ul').append( $('<li>').text(v) );
        });
    }

})(jQuery);