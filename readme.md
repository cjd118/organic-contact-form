# Organic Contact Form

### Adding a contact form

The contact form is built as a shortcode so can be added anywhere a shortcode is supported with [organic-contact-form].

### Exporting CSV

See sub menu option on contact form submissions admin page.

### Modifying js/sass

Install gulp globally then: 
`npm install`

`gulp watch`